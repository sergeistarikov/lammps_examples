## lammps-script for calculation of melting temperature at P = 0
## Sergei Starikov, ICAMS
## citation: D. Smirnova, S. Starikov, G.L. Leines et al. Phys Rev Mat 4, 013605 (2020)

units		metal						## E in [eV], t in [ps], P in [10000 Pa], l in [A]
atom_style	atomic						## mass x y z vx vy vz 

neighbor	0.5 bin						## neighbor list, skin = 0.5 Angstrom
neigh_modify	every 2 delay 10 check yes			## update of neighbor list

############################## main variables

variable	a equal 3.20					## variable
variable	Tm equal 3700					## expected temperature (may be incorrect)

##############################

lattice		bcc $a						## set of lattice
region		box block 0 26 0 7 0 7				## set of region, 26x7x7 a^3 [A^3]
create_box	3 box						## creation of the simulation box, 3 types of atoms
create_atoms	1 box						## the filling of the box by atoms (type 1)

######### composition for eqiatomic binary alloy

#set		group all type/ratio 2 0.5 2345	## set of composition  ### 
#set		group all type/ratio 3 0.5 1395	## set of composition  ###

#########

mass		1 184.0						## W ## set atomic mass for 1st type (molar mass in [g/mol])
mass		2 96.0						## Mo ## set atomic mass for 2nd type (molar mass in [g/mol])
mass		3 92.0						## Nb ## set atomic mass for 3-type (molar mass in [g/mol])

variable	T0 equal 2.0*0.8*${Tm}				## temperature for setting

variable	T1 equal 0.8*${Tm}				## initial temperature for the heating
variable	T2 equal 1.3*${Tm}				## final temperature for the heating

velocity	all create ${T0} 87287				## initial velocities with average kinetic energy K = (3/2)kT with T = T0 (Gaussian destribution)  
timestep	0.0005						## timestep = 0.0005*ps = 0.5 fs

pair_style	adp						## potential 
pair_coeff	* * ./W_Mo_Nb.final.adp.txt W Mo Nb		## potential

dump		snap all cfg 10000 ./dump/fe.*.cfg mass type xs ys zs type	## !!! be careful, don't forget to clean the folder 
dump_modify	snap element W Mo Nb					## types of atoms in cfg-file

thermo		100						## writing of MD info in log-file every 100 steps								
thermo_style	custom step temp pe ke etotal pxx pyy pzz press vol lx ly lz

region		r_msd block 14 22 -1000 1000 -1000 1000		## block for MSD calculation
group		g_msd region r_msd				## all atoms in {r_msd} region -> {g_msd} group

################## preparation of the initial state

fix		init all npt temp ${T1} ${T1} 2.0 aniso 0.0 0.0 2.0	## {fix}: some action on atoms during  {run} command ## MD in NPT conditions (P=0) 
run		20000						## may be decreased (if necessary)
unfix		init						## turn off

region		del block -100 10 -1000 1000 -1000 1000		## creation of free surfaces
delete_atoms	region del					## creation of free surfaces

velocity    all create ${T1}  45321  

fix		eq all nve					## equilibration
run		10000						## may be decreased (if necessary)
unfix		eq						## turn off

################## main calculation

compute		msd g_msd msd com yes				## compute ID group-ID style ## calculation of MSD for {g_msd} atoms ## {com yes}: drift in the center-of-mass is subtracted 
thermo_style	custom step temp pe ke etotal pxx pyy pzz press vol ly lz c_msd[1] c_msd[2] c_msd[3] c_msd[4]

fix		heat all npt temp ${T1} ${T2} 2.0 y 0.0 0.0 2.0 z 0.0 0.0 2.0	## MD in NVP conditions: heating from T1 to T2 with K ~ 2 [K/ps]
run		800000						## number of steps ## may be decreased/increased (if necessary)

#write_data	./data.txt


